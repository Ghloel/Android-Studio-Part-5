package com.example.memo.Interfaces;

import android.view.View;

public interface RecyclerViewClickListenerAdd {
    void onClick(View view, int position);
}
