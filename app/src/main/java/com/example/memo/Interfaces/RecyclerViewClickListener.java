package com.example.memo.Interfaces;

import android.view.View;

public interface RecyclerViewClickListener {
    void onClick(View view, int position);
}
