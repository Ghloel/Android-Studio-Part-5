package com.example.memo.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.memo.Adapters.RecyclerViewAdapter;
import com.example.memo.Interfaces.RecyclerViewClickListener;
import com.example.memo.Listeners.RecyclerTouchListener;
import com.example.memo.Models.DataModel;
import com.example.memo.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<DataModel> dataModelList;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Instantiate the recyclerView
        recyclerView = findViewById(R.id.recyclerView);

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setFocusable(false);
        setData();
        setDataToRecyclerView();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                String title = dataModelList.get(position).getTitle();
                String text = dataModelList.get(position).getText();
                int id = dataModelList.get(position).getId();
                Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                intent.putExtra("title", title);
                intent.putExtra("text", text);
                intent.putExtra("id", id);
                MainActivity.this.startActivity(intent);
            }
        }));

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddActivity.class);
                intent.putExtra("alldata" , dataModelList.toString());
                MainActivity.this.startActivity(intent);
            }
        });

    }

    private void setData() {
        dataModelList = new ArrayList<>();
        dataModelList.add(new DataModel(1, "Memo 1", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet odio ac tellus cursus ullamcorper. Praesent nulla est, pulvinar ac scelerisque nec, congue eget sapien. Nulla facilisi. Quisque blandit, magna nec facilisis tincidunt, nisi sapien laoreet sem, in efficitur velit mi in leo. Sed quis lectus efficitur, congue libero semper, auctor urna. Nunc eros sapien, aliquam in tellus eu, vulputate cursus eros. Etiam sed tempus odio, vitae tempus justo. Vestibulum mollis nulla molestie gravida elementum. Etiam justo massa, aliquet posuere sapien at, porta lacinia est."));
        dataModelList.add(new DataModel(2, "Memo 2", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet odio ac tellus cursus ullamcorper. Praesent nulla est, pulvinar ac scelerisque nec, congue eget sapien. Nulla facilisi. Quisque blandit, magna nec facilisis tincidunt, nisi sapien laoreet sem, in efficitur velit mi in leo. Sed quis lectus efficitur, congue libero semper, auctor urna. Nunc eros sapien, aliquam in tellus eu, vulputate cursus eros. Etiam sed tempus odio, vitae tempus justo. Vestibulum mollis nulla molestie gravida elementum. Etiam justo massa, aliquet posuere sapien at, porta lacinia est."));
        dataModelList.add(new DataModel(3, "Memo 3", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet odio ac tellus cursus ullamcorper. Praesent nulla est, pulvinar ac scelerisque nec, congue eget sapien. Nulla facilisi. Quisque blandit, magna nec facilisis tincidunt, nisi sapien laoreet sem, in efficitur velit mi in leo. Sed quis lectus efficitur, congue libero semper, auctor urna. Nunc eros sapien, aliquam in tellus eu, vulputate cursus eros. Etiam sed tempus odio, vitae tempus justo. Vestibulum mollis nulla molestie gravida elementum. Etiam justo massa, aliquet posuere sapien at, porta lacinia est."));
        dataModelList.add(new DataModel(4, "Memo 4", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet odio ac tellus cursus ullamcorper. Praesent nulla est, pulvinar ac scelerisque nec, congue eget sapien. Nulla facilisi. Quisque blandit, magna nec facilisis tincidunt, nisi sapien laoreet sem, in efficitur velit mi in leo. Sed quis lectus efficitur, congue libero semper, auctor urna. Nunc eros sapien, aliquam in tellus eu, vulputate cursus eros. Etiam sed tempus odio, vitae tempus justo. Vestibulum mollis nulla molestie gravida elementum. Etiam justo massa, aliquet posuere sapien at, porta lacinia est."));
        dataModelList.add(new DataModel(5, "Memo 5", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet odio ac tellus cursus ullamcorper. Praesent nulla est, pulvinar ac scelerisque nec, congue eget sapien. Nulla facilisi. Quisque blandit, magna nec facilisis tincidunt, nisi sapien laoreet sem, in efficitur velit mi in leo. Sed quis lectus efficitur, congue libero semper, auctor urna. Nunc eros sapien, aliquam in tellus eu, vulputate cursus eros. Etiam sed tempus odio, vitae tempus justo. Vestibulum mollis nulla molestie gravida elementum. Etiam justo massa, aliquet posuere sapien at, porta lacinia est."));
        dataModelList.add(new DataModel(6, "Memo 6", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet odio ac tellus cursus ullamcorper. Praesent nulla est, pulvinar ac scelerisque nec, congue eget sapien. Nulla facilisi. Quisque blandit, magna nec facilisis tincidunt, nisi sapien laoreet sem, in efficitur velit mi in leo. Sed quis lectus efficitur, congue libero semper, auctor urna. Nunc eros sapien, aliquam in tellus eu, vulputate cursus eros. Etiam sed tempus odio, vitae tempus justo. Vestibulum mollis nulla molestie gravida elementum. Etiam justo massa, aliquet posuere sapien at, porta lacinia est."));
        dataModelList.add(new DataModel(7, "Memo 7", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet odio ac tellus cursus ullamcorper. Praesent nulla est, pulvinar ac scelerisque nec, congue eget sapien. Nulla facilisi. Quisque blandit, magna nec facilisis tincidunt, nisi sapien laoreet sem, in efficitur velit mi in leo. Sed quis lectus efficitur, congue libero semper, auctor urna. Nunc eros sapien, aliquam in tellus eu, vulputate cursus eros. Etiam sed tempus odio, vitae tempus justo. Vestibulum mollis nulla molestie gravida elementum. Etiam justo massa, aliquet posuere sapien at, porta lacinia est."));
        dataModelList.add(new DataModel(8, "Memo 8", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras laoreet odio ac tellus cursus ullamcorper. Praesent nulla est, pulvinar ac scelerisque nec, congue eget sapien. Nulla facilisi. Quisque blandit, magna nec facilisis tincidunt, nisi sapien laoreet sem, in efficitur velit mi in leo. Sed quis lectus efficitur, congue libero semper, auctor urna. Nunc eros sapien, aliquam in tellus eu, vulputate cursus eros. Etiam sed tempus odio, vitae tempus justo. Vestibulum mollis nulla molestie gravida elementum. Etiam justo massa, aliquet posuere sapien at, porta lacinia est."));
    }

    private void setDataToRecyclerView() {
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(dataModelList);
        recyclerView.setAdapter(adapter);
    }
}
