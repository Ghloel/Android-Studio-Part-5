package com.example.memo.Models;

public class DataModel {
    private int id;
    private String title;
    private String text;

    //Default Constructor
    public DataModel() {
    }

    //Constructor with parameter
    public DataModel(int id, String title, String text) {
        this.id = id;
        this.title = title;
        this.text = text;
    }

    //Getters and Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "DataModel{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
